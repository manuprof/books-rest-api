<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AuthorController extends Controller
{
    
    /** aggiunge un nuovo autore */
    public function add(Request $request)
    {
        $name = $request->input('name');
        $born = $request->input('born');
        $nationality = $request->input('nationality');
                
        $results = app('db')->insert(
            "insert into authors(name,born,nationality) values('$name','$born','$nationality') "
        );
        // ritorno 201 created
        return new Response(null, 201);
    }

    /** aggiorno un autore esistente per id */
    public function update(Request $request,$id)
    {
        $name = $request->input('name');
        $born = $request->input('born');
        $nationality = $request->input('nationality');
                
        $results = app('db')->update(
            "UPDATE authors SET name='$name', born='$born',nationality='$nationality' WHERE id=$id"
        );
        // ritorno 200 OK
        return new Response(null, 200);
    }

       /** elimino un autore esistente per id */
       public function delete(Request $request,$id)
       {
                  
           $results = app('db')->delete(
               "DELETE FROM authors WHERE id=$id"
           );
           // ritorno 204 no content
           return new Response(null, 204);
       }

}
