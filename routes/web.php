<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return 'ciao a tutti';
});

// prende l'elenco degli autori
$router->get('/authors', function () use ($router) {
    // seleziono tutti gli autori
    $results = app('db')->select("SELECT * FROM authors");
    count($results);
    return $results;
});

/** inserire un nuovo autore
 *  usando il controller AuthorController ed il metodo add
*/
$router->post('/authors', 'AuthorController@add');

/** 
 * Fare la update
 * Aggiorno un autore nel database selezionandolo per id
*/
$router->put('/authors/{id}', 'AuthorController@update');

/* Fare la delete
 * Elimino un record selezionando per id
*/
$router->delete('/authors/{id}', 'AuthorController@delete');

// fare la select per identity